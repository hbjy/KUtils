# Commands

## Key
| Symbol     | Meaning                    |
| ---------- | -------------------------- |
| (Argument) | This argument is optional. |

## Data
| Commands | Arguments | Description                                                                           |
| -------- | --------- | ------------------------------------------------------------------------------------- |
| DataSave | <none>    | This command lets you modify a Data object's contents.                                |
| DataSee  | <none>    | This command demonstrates loading and injecting Data objects by viewing its contents. |

## ServicesDemo
| Commands     | Arguments | Description              |
| ------------ | --------- | ------------------------ |
| DependsOnAll | <none>    | I depend on all services |

## Misc
| Commands    | Arguments | Description             |
| ----------- | --------- | ----------------------- |
| SomeCommand | <none>    | No Description Provider |

## Utility
| Commands         | Arguments          | Description                                    |
| ---------------- | ------------------ | ---------------------------------------------- |
| Add              | Integer, Integer   | Add two numbers together                       |
| ConversationTest | <none>             | Test the implementation of the ConversationDSL |
| DisplayEmbed     | <none>             | Display an example embed.                      |
| DisplayMenu      | <none>             | Display an example menu.                       |
| Echo             | Text               | No Description Provider                        |
| Help             | (Command)          | Display a help menu.                           |
| NumberOrWord     | Integer \| Word    | Enter a word or a number                       |
| OptionalAdd      | Integer, (Integer) | Add two numbers together                       |
| Version, Ver, V  | <none>             | A command which will show the version.         |

